
.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://kolektiva.social/@iranluttes"></a>
   <a rel="me" href="https://kolektiva.social/@kurdistanluttes"></a>

.. _linkertree_iranluttes:

=====================================================================
Liens **iranluttes**
=====================================================================

🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی (Persan) #ZanZendegiÂzâdi 🇮🇷
🇫🇷 Femme, Vie, Liberté #FemmeVieLiberte (Français, 🇫🇷)
🇮🇹 Donna, Vita, Libertà (Italien, 🇮🇹 )
🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
🇪🇸 Mujer Vida Libertad

Agenda
=========

- https://openagenda.com/fr/evenements-grenoble-luttes

iranluttes 2022-2025
=====================================

- https://iran.frama.io/luttes-2025/
- https://iran.frama.io/luttes-2024/
- https://iran.frama.io/luttes-2023/
- https://iran.frama.io/luttes/ (chants, films, livres, slogans)
- https://iran.frama.io/media/ (images macval)
- https://raindrop.io/iranluttes


iranluttes sur pixelfed
============================

- https://pixelfed.fr/users/Iranluttes.atom
- https://pixelfed.fr/Iranluttes

iranluttes sur loops
========================

- https://loops.video/@iranluttes


Exposition "Femme-Vie-Liberte" dans la région Grenobloise
==============================================================

- https://femme-vie-liberte.frama.io/expositions-2025/arthaud-grenoble
- https://femme-vie-liberte.frama.io/expositions-2024/seyssinet-pariset/
- https://femme-vie-liberte.frama.io/expositions-2023/grenoble/

La chaine Youtube https://www.youtube.com/@iranluttes
========================================================

- https://www.youtube.com/@iranluttes
- https://www.youtube.com/@iranluttes/about


iranluttes sur Instagram
==========================

- https://www.instagram.com/iranluttes/



Expositions, oeuvres d'artistes
========================================

Dessins de Bahareh Makrami
---------------------------

- https://bahareh-akrami.frama.io/iran/



Luttes féministes au Kurdistan
=========================================

- https://kurdistan.frama.io/linkertree/

Luttes féministes au Rojava
=========================================

- https://rojava.frama.io/luttes/


**RAAR (Réseau d'Actions contre l'Antisémitisme et tous les Racismes)**
==========================================================================

- https://antiracisme.frama.io/linkertree/

Liens Grenoble
====================

- https://grenoble.frama.io/linkertree/


Pour la paix
===============

- https://luttes.frama.io/pour/la-paix/linkertree

Luttes féministes en Iran
=========================================

- https://iran.frama.io/linkertree/



iranluttes sur Mastodon https://kolektiva.social/@iranluttes
==================================================================

- https://kolektiva.social/@iranluttes


Liste de diffusion iranluttes-infos
=====================================

- https://framalistes.org/sympa/info/iranluttes-infos


Iranluttes sur mobilizon (en test)
=====================================

- https://mobilizon.chapril.org/@iranluttes38

Kurdistan, AIAK
================================

- https://kurdistan.frama.io/linkertree/ 
- https://aiak.frama.io/aiak-info/
- https://rojava.frama.io/luttes/

Flux Web RSS
===============

- https://kolektiva.social/@iranluttes.rss

Exemples de tags RSS

- https://framapiaf.org/web/tags/MahsaAmini.rss
- https://framapiaf.org/web/tags/iran.rss
- https://framapiaf.org/web/tags/kurdistan.rss
- https://framapiaf.org/web/tags/rojava.rss


Mastowall
-----------

- https://rstockm.github.io/mastowall/?hashtags=ArmitaGaravand&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=WomenLifeFreedom&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=iranrevolution&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=mahsaamini&server=https://framapiaf.org

